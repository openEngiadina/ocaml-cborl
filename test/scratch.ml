(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let () = Random.self_init ()
let item = Cborl.Integer (Z.of_string "-18446744073709551617")

(* Cborl.(
 *   Array
 *     [
 *       Integer (Z.of_int 0);
 *       Null;
 *       Undefined;
 *       TextString "Hello World!";
 *       ByteString "CBOR BLUP BLUP";
 *       Map [];
 *       Bool true;
 *       Bool false;
 *       Array [ Integer (Z.of_int 0xfffff) ];
 *       Array [];
 *     ]) *)

let z_numbytes z =
  let numbits = Z.numbits z |> Float.of_int in
  numbits /. 8.0 |> ceil |> int_of_float

let () =
  (* let z = Z.of_bits (String.make 8 @@ Char.chr 200) in *)
  let z = Z.of_string "-18446744073709551616" in
  let s = Z.to_bits z in
  Format.printf "Z: %a (size: %d) %d\n" Z.pp_print z (z_numbytes z)
    (String.length s);

  Format.printf "CBOR: %a\n" Cborl.pp item;
  Format.printf "Signals: @[%a@]\n"
    Fmt.(seq ~sep:semi Cborl.Signal.pp)
    (Cborl.Signal.of_item item);

  let s = item |> Cborl.write |> String.of_seq in
  Format.printf "Encoded: %a\n" Fmt.(on_string @@ hex ()) s;

  let decoded = s |> String.to_seq |> Cborl.Signal.read in
  Format.printf "Stream: @[%a@]\n" Fmt.(seq ~sep:semi Cborl.Signal.pp) decoded;

  let signals_decoded = s |> String.to_seq |> Cborl.Signal.read in
  Format.printf "Stream: @[%a@]\n"
    Fmt.(seq ~sep:semi Cborl.Signal.pp)
    signals_decoded;

  let decoded = signals_decoded |> Cborl.Signal.to_items in
  Format.printf "Item: @[%a@]\n" Fmt.(seq ~sep:semi Cborl.pp) decoded;

  Format.printf "\n\nIndefinite-length:\n";

  let a = [ Cborl.Integer (Z.of_int 2) ] in
  let signals = Cborl.Signal.indefinite_length_array (List.to_seq a) in
  Format.printf "Signals: @[%a@]\n" Fmt.(seq ~sep:semi Cborl.Signal.pp) signals;

  let s = signals |> Cborl.Signal.write |> String.of_seq in
  Format.printf "Encoded: %a\n" Fmt.(on_string @@ hex ()) s;

  let signals_decoded = s |> String.to_seq |> Cborl.Signal.read in
  Format.printf "Stream: @[%a@]\n"
    Fmt.(seq ~sep:semi Cborl.Signal.pp)
    signals_decoded;

  let decoded = signals_decoded |> Cborl.Signal.to_items in
  Format.printf "Item: @[%a@]\n" Fmt.(seq ~sep:semi Cborl.pp) decoded;

  ()
