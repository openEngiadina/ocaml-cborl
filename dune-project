(lang dune 3.2)

; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: CC0-1.0

(name cborl)
(version 0.1.0)

(generate_opam_files true)

(source (uri "git+https://inqlab.net/git/ocaml-cborl.git"))
(homepage "https://inqlab.net/git/ocaml-cborl.git")
(authors "pukkamustard <pukkamustard@posteo.net>")
(maintainers "pukkamustard <pukkamustard@posteo.net>")
(bug_reports "mailto:pukkamustard@posteo.net")

(license AGPL-3.0-or-later)

(package
 (name cborl)
 (synopsis "CBOR library")
 (description "The Concise Binary Object Representation (CBOR), as
 specified by RFC 8949, is a binary data serialization format.  CBOR
 is similar to JSON but serializes to binary which is smaller and
 faster to generate and parse.  This package provides an OCaml
 implementation of CBOR." )
 (depends
   (ocaml (>= "4.14.0"))
   (zarith (>= "1.12"))
   (fmt (>= "0.8.7"))
   (alcotest :with-test)
   (qcheck :with-test)
   (qcheck-alcotest :with-test)))
