# ocaml-cborl

An implementation of the Concise Binary Object Representation (CBOR) as specified by [RFC 8949](https://www.rfc-editor.org/rfc/rfc8949.html) for OCaml.

It can process CBOR data items without a complete in-memory representation of the data (see `Cborl.Signal`).

## Scope

This library supports the CBOR basic generic data model (see [CBOR Data Models](https://www.rfc-editor.org/rfc/rfc8949.html#name-cbor-data-models)) modulo following TODOs:

 - [ ] Support for IEEE 754 Half-Precision Float (16 bits). Support for decoding such values could be added with some minimal OCaml code (see https://www.rfc-editor.org/rfc/rfc8949.html#name-half-precision)
 - [ ] Indefinite-lenght byte and text strings
 
In addition to the basic generic data model this implementation supports:

- Simple values for `false`, `true`, `null` and `undefined`
- Bignums: `Cborl.Integer z` items with `z` that is larger than what can be encoded in 8 bytes will be encoded as Bignums (tag 2 or 3 followed by a bytestring; see [Section 3.4.3 of RFC 8949](https://www.rfc-editor.org/rfc/rfc8949.html#name-bignums)).

Contributions are very welcome!

## Acknowledgments

ocaml-cborl was initially developed for the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLNet Foundation](https://nlnet.nl/) trough the [NGI0 Discovery Fund](https://nlnet.nl/discovery/).

## License

[AGPL-3.0-or-later](./LICENSES/AGPL-3.0-or-later.txt)
